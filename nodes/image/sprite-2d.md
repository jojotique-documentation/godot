# Sprite2D

Le `Sprite2D` est un type de nœud qui permet d'afficher une image à l'écran. Il est souvent utilisé pour afficher des
personnages, des objets ou des décors.

:::tip
Quand on charge des images de type "Pixel art", il est préférable de les charger en mode "Nearest" pour éviter les
flous. Pour ce faire, il suffit d'aller dans "Project" → "Project Settings" -> "Rendering" → "Texture" et choisir
"Nearest" dans "Default Texture Filter".
:::

## TileSet

Le `TileSet` est un type d'image qui permet de découper une image en plusieurs morceaux. Exemple :

![TileSet](/tileset-player.png)

### Découpage

Pour récupérer une seule image du `TileSet`, il faut aller dans le menu "Inspector" → "Sprite2D" → "Animation".
Ensuite, on renseigne le nombre de colonnes (Vframes) et de lignes (Hframes) de l'image.