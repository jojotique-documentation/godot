# CharacterBody2D

Le `CharacterBody2D` est un type de nœud qui permet de gérer les collisions et la physique d'un personnage.

:::info
Il doit avoir une enfant de type [`CollisionShape2D`](/nodes/collision/collision-shape-2d)
ou [`CollisionPolygon2D`](/nodes/collision/collision-polygon-2d) pour gérer les collisions avec les autres objets.
:::