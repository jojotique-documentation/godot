# Camera2D

La `Camera2D` est un type de nœud qui permet de gérer la caméra du jeu. Quand on l'ajoute et qu'on dézoome assez, on
peut voir un rectangle violet qui représente la zone de la caméra.

![Rectangle violet représentant la zone de la caméra](/camera-2d-example.png)