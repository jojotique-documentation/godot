# Création de notre personnage

On ajoute un personnage principal dans la scène principale.

## Type de nœud

Pour ce personnage, pour s'éviter de coder pleins de choses, notamment la gestion des collisions et de la physique, on
va utiliser un [`CharacterBody2D`](/nodes/master-node/character-body-2d).

## CollisionShape2D

Dans un RPG 2D, comme la tête du personnage va parfois passer au-dessus d'éléments qui contiennent
des [`CollisionShape2D`](/nodes/collision/collision-shape-2d), il est préférable de ne mettre un `CollisionShape2D` que
sur le bas du corps du personnage.

![Exemple de placement du CollisionShape2D](/player-collision-shape-example.png)

## Caméra

Pour que la caméra suive le personnage, il suffit d'ajouter une [`Camera2D`](/nodes/camera/camera-2d) à la scène
principale et d'ajouter le personnage dans le rectangle violet.

:::tip
On peut mettre le zoom à la valeur qu'on souhaite. Dans un RPG 2D, souvent on peut mettre le zoom à 3 pour que le
personnage principal soit bien visible.
:::

:::warning
Si on laisse les valeurs par défaut, quand on met la fenêtre en plein écran, le personnage va être plus petit. Pour
éviter ça, on peut aller dans "Project" → "Project Settings" → "Display" → "Window" et mettre la valeur de "
Stretch" → "Mode" à "Viewport".
:::

## Le script

Création d'un script en appuyant sur :

![Bouton pour créer un script](/add-script-button.png)

### Gérer les déplacements

On ajoute une variable `speed` pour gérer la vitesse du personnage et une variable `velocity` pour gérer la direction du
personnage.

```gdscript
extends CharacterBody2D

@export var speed: int = 200 # [!code ++]

var input_movement: Vector2 = Vector2.ZERO # [!code ++]
```

:::tip
On peut "exporter" la variable `speed` pour pouvoir la modifier dans l'éditeur.

![Exemple de l'export de la variable speed](/var-exported-example.png)
:::

### Détection des touches

On ajoute une fonction `_physics_process` et à l'intérieur, on ajoute la détection des touches pour gérer les
déplacements.

```gdscript
extends CharacterBody2D

@export var speed: int = 200

var input_movement: Vector2 = Vector2.ZERO


func physics_process(delta: float) -> void: # [!code ++]
    pass # [!code ++]
```

:::tip
La fonction `_physics_process` est appelée à chaque frame, donc 60 fois par seconde par défaut (60 FPS).
:::

Puis on ajoute la détection des touches pour gérer les déplacements.

```gdscript
extends CharacterBody2D

@export var speed: int = 200

var input_movement: Vector2 = Vector2.ZERO


func physics_process(delta: float) -> void:
    pass # [!code --]
    input_movement = Input.get_vector('ui_left', 'ui_right', 'ui_up', 'ui_down') # [!code ++]
```

:::warning
Attention, l'ordre des touches dans `Input.get_vector` est important. Si on met 'ui_down' avant 'ui_up', le personnage
va se déplacer vers le bas même si on appuie sur la touche pour aller vers le haut.
:::

Puis, on vérifie si le personnage se déplace et on lui donne une vélocité.

```gdscript
extends CharacterBody2D

@export var speed: int = 200

var input_movement: Vector2 = Vector2.ZERO


func physics_process(delta: float) -> void:
    input_movement = Input.get_vector('ui_left', 'ui_right', 'ui_up', 'ui_down')

    if input_movement != Vector2.ZERO: # [!code ++]
        velocity = input_movement.normalized() * speed # [!code ++]

    if input_movement == Vector2.ZERO: # [!code ++]
        velocity = Vector2.ZERO # [!code ++]
```

:::tip
`velocity` est une variable de `CharacterBody2D` qui permet de gérer la direction du personnage. Elle est déclarée dans
la classe `CharacterBody2D`.
:::

Pour finir, on demande à Godot de mettre à jour la position du personnage.

```gdscript
extends CharacterBody2D

@export var speed: int = 200

var input_movement: Vector2 = Vector2.ZERO


func physics_process(delta: float) -> void:
    input_movement = Input.get_vector('ui_left', 'ui_right', 'ui_up', 'ui_down')

    if input_movement != Vector2.ZERO:
        velocity = input_movement.normalized() * speed

    if input_movement == Vector2.ZERO:
        velocity = Vector2.ZERO

    move_and_slide(velocity) # [!code ++]
```