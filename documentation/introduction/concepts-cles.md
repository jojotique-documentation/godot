# Concepts clés de Godot

Les moteurs de jeu s'articulent autour d'abstractions. Dans Godot, un jeu est un [**arbre**](#larbre-de-scene) de
[**nœuds**](#les-nœuds), que vous rassemblez dans des [**scènes**](#les-scenes). Les [**nœuds**](#les-nœuds)
communiquent alors entre eux en utilisant des [**signaux**](#les-signaux).

## Les scènes

Dans Godot, une scène peut être :

- Un niveau de jeu
- Un personnage ou une arme
- Une maison
- Un menu
- ou n'importe quoi d'autre

Elles sont flexibles, et peuvent être associées pour former des tous. Par exemple, on peut mettre un personnage comme
enfant d'un niveau de jeu.
![Exemple d'un personnage ajouté dans une scène](/intro-perso_scene.png)

Elles peuvent être comparées aux prefabs d'Unity ou aux scènes de Unreal Engine.

## Les nœuds

Les nœuds sont les éléments de base de Godot. Ils sont les briques de construction des scènes. Chaque nœud a un rôle,
exemple :

- `Sprite` : affiche une image
- `Camera2D` : affiche la scène depuis un point de vue
- `CollisionShape2D` : définit une zone de collision
- ...

![Exemple de la liste des nœuds d'une scène](/intro-scene_noeuds.png)
Quand on explore une scène, on voit l'ensemble des nœuds, alors que dans l'affichage des scènes, on ne voit pas le
détail de ces dernières.

## L'arbre de scène

L'arbre de scènes est la structure du jeu, il regroupe toutes les scènes, comme une scène est un arbre de nœuds.

## Les signaux

Les signaux sont des messages que les nœuds peuvent envoyer et recevoir. Par exemple, un nœud `Button` envoie un signal
pour lancer le jeu quand il est cliqué.

Les signaux sont utilisés pour communiquer entre les nœuds, sans qu'ils aient besoin de se connaître. Cela permet de les
rendre plus modulaires et réutilisables.

