# Présentation de Godot

## Le gestionnaire de projets

Lorsque vous lancez Godot, vous arrivez sur le gestionnaire de projets. C'est ici que vous pouvez créer un nouveau
projet, ouvrir un projet existant, ou importer un projet depuis un dépôt Git.

![Présentation du gestionnaire de projets](/intro-project_manager.png)

## L'interface de l'éditeur

L'interface de Godot est divisée en plusieurs fenêtres. Chaque fenêtre a un rôle spécifique.

![Présentation de l'interface de l'éditeur](/intro-editeur_presentation.png)

### La barre de menus

![Présentation de la barre des menus](/intro-barre_menus.png)

Elle comporte :

- les menus
- les écrans de travail
- les boutons de contrôle (pour lancer le jeu, sauvegarder, etc.)

### La fenêtre de la scène ou viewport et la barre d'outils

![Présentation de la fenêtre de la scène](/intro-viewport.png)

La fenêtre de la scène est l'endroit où vous allez placer vos objets et les organiser.

La barre d'outils va être différente en fonction du type de vue que vous avez sélectionnée. Par exemple :

- en vue 2D
  ![Présentation de la barre d'outils 2D](/intro-toolbar_2d.png)
- en vue 3D
  ![Présentation de la barre d'outils 3D](/intro-toolbar_3d.png)

### Les docks

Autour du viewport, vous avez plusieurs docks :

- la fenêtre de la scène
  ![Présentation de la fenêtre de la scène](/intro-dock_scene.png)
- le système de fichiers
  ![Présentation du système de fichiers](/intro-dock_filesystem.png)
- l'inspecteur
  ![Présentation de l'inspecteur](/intro-dock_inspector.png)

### Le panneau inférieur

Le panneau inférieur est sous le viewport. Il est l'hôte de la console de débogage, de l'éditeur d'animation, de
l'éditeur de script, de l'éditeur audio...
![Présentation du panneau inférieur](/intro-bottom_panel.png)

### L'écran script

C'est un éditeur complet avec un débogueur, une riche auto-complétion et une référence de code intégrée.

### Autre

> [!TIP]
> On peut ouvrir l'aide de partout dans l'éditeur en appuyant sur `F1`. 