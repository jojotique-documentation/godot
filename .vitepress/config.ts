import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: 'Godot',
  description: 'Documentation pour godot',

  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Documentation', link: '/documentation/introduction/concepts-cles' },
      { text: 'Nœuds', link: '/nodes/' },
    ],

    sidebar: {
      '/nodes/': [
        { text: 'Sommaire', link: '/nodes/' },
        {
          text: 'Animation',
          items: [
            { text: 'AnimationPlayer', link: '/nodes/animation/animation-player' },
            { text: 'AnimationTree', link: '/nodes/animation/animation-tree' },
          ],
        },
        {
          text: 'Camera',
          items: [
            { text: 'Camera2D', link: '/nodes/camera/camera-2d' },
          ],
        },
        {
          text: 'Collision',
          items: [
            { text: 'CollisionShape2D', link: '/nodes/collision/collision-shape-2d' },
            { text: 'CollisionPolygon2D', link: '/nodes/collision/collision-polygon-2d' },
          ],
        },
        {
          text: 'Image',
          items: [
            { text: 'Sprite2D', link: '/nodes/image/sprite-2d' },
          ],
        },
        {
          text: 'Noeud maître',
          items: [
            { text: 'Area2D', link: '/nodes/master-node/area-2d' },
            { text: 'CharacterBody2D', link: '/nodes/master-node/character-body-2d' },
            { text: 'Node2D', link: '/nodes/master-node/node-2d' },
          ],
        },
      ],
      '/documentation/': [
        {
          text: 'Introduction',
          items: [
            { text: 'Concepts clés', link: '/documentation/introduction/concepts-cles' },
            { text: 'Présentation', link: '/documentation/introduction/presentation' },
          ],
        },
        {
          text: 'Créer un RPG en 2D avec Godot 4 (Udemy)',
          link: '/documentation/create-rpg-fr/',
          items: [
            {
              text: 'Les fondations de notre jeu',
              items: [
                {
                  text: 'Création de notre personnage',
                  link: '/documentation/create-rpg-fr/game-fondation/character-creation',
                },
                {
                  text: 'Création des animations pour notre personnage',
                  link: '/documentation/create-rpg-fr/game-fondation/character-animation-creation',
                },
              ],
            },
            {
              text: 'Ajouter des mécaniques de jeu',
              items: [
                { text: 'Sauter', link: '/documentation/create-rpg-fr/game-mechanics' },
              ],
            },
            {
              text: 'Interface',
              items: [
                { text: 'Création de l\'interface', link: '/documentation/create-rpg-fr/interface' },
              ],
            },
          ],
        },
      ],
    },
  },
})
